import { PrismaClient } from '@prisma/client'
import * as bcrypt from 'bcryptjs'

const prisma = new PrismaClient()
async function main() {
  const user = await prisma.user.create({
    data: {
      email: `admin@topfood.com`,
      name: 'Admin ',
      password: await bcrypt.hash('admin', 10),
      role: 'ADMIN',
      resetPasswordToken: '123',
      validateEmailToken: '',
    },
  })

  console.log({ user })
}
main()
  .catch((e) => {
    console.error(e)
    process.exit(1)
  })
  .finally(async () => {
    await prisma.$disconnect()
  })
