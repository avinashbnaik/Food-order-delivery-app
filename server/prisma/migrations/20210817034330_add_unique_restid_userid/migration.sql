/*
  Warnings:

  - A unique constraint covering the columns `[restaurantId,userId]` on the table `BlockedUser` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "BlockedUser.restaurantId_userId_unique" ON "BlockedUser"("restaurantId", "userId");
