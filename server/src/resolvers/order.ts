import { Context } from '../model/appInterface'
import { PubSub, withFilter } from 'apollo-server'
const pubsub = new PubSub()
const ORDER_CREATED = 'ORDER_CREATED'
const ORDER_UPDATED = 'ORDER_UPDATED'

import utils from '../utils'
export const resolvers = {
  Subscription: {
    orderCreated: {
      subscribe: withFilter(
        () => pubsub.asyncIterator([ORDER_CREATED]),
        (payload: any, variables: any): any => {
          return variables.restaurantIds.includes(
            payload.orderCreated.restaurantId,
          )
        },
      ),
    },
    orderUpdated: {
      subscribe: withFilter(
        () => pubsub.asyncIterator([ORDER_UPDATED]),
        (payload: any, variables: any): any => {
          return payload.orderUpdated.id === variables.orderId
        },
      ),
    },
  },
  Query: {
    orders: async (parent, args, ctx: Context) => {
      const ordrs = await ctx.prisma.order.findMany({
        where: { restaurantId: parent.id },
        orderBy: [
          {
            updatedAt: 'desc',
          },
        ],
      })
      return ordrs
    },
    orderById: (parent, args, ctx: Context) => {
      return ctx.prisma.order.findUnique({ where: { id: args.id } })
    },
  },
  OrderItemType: {
    //get meal info from Meal table
    meal: async (parent, args, ctx: Context) => {
      const meal = await ctx.prisma.meal.findUnique({
        where: {
          id: parent.mealId,
        },
      })
      return meal
    },
  },
  Order: {
    // get meals from the order id
    items: async (parent, args, ctx: Context) => {
      const orderItems = await ctx.prisma.orderItem.findMany({
        where: {
          orderId: parent.id,
        },
      })
      return orderItems
    },
    // get restaurant info from the order
    restaurant: async (parent, args, ctx: Context) => {
      const restaurant = await ctx.prisma.restaurant.findUnique({
        where: {
          id: parent.restaurantId,
        },
      })
      return restaurant
    },
    // get user info from the order
    user: async (parent, args, ctx: Context) => {
      const user = await ctx.prisma.user.findUnique({
        where: {
          id: parent.userId,
        },
      })
      return user
    },
    // get order history
    history: async (parent, args, ctx: Context) => {
      const orderItems = await ctx.prisma.orderHistory.findMany({
        where: {
          orderId: parent.id,
        },
      })
      return orderItems
    },
  },
  Mutation: {
    createOrder: async (parent, args, ctx: Context) => {
      const user = await utils.getUser(ctx)
      if (user.role !== 'USER') {
        throw new Error('User not authorized to perform this action')
      }
      if (!user) {
        throw new Error('User is not registered')
      }
      const order = await ctx.prisma.order.create({
        data: {
          restaurantId: args.restaurantId,
          userId: user.userId,
          items: {
            create: args.items,
          },
        },
      })

      pubsub.publish(ORDER_CREATED, { orderCreated: order })
      return {
        id: order.id,
      }
    },
    updateOrder: async (parent, args, ctx: Context) => {
      const user = await utils.getUser(ctx)
      if (!user) {
        throw new Error('Invalid user/User not registered')
      }
      if (user.role !== 'USER' && args.status === 'CANCELLED') {
        throw new Error('User not authorized to perform this action')
      }
      const order = await ctx.prisma.order.update({
        data: { status: args.status },
        where: {
          id: args.orderId,
        },
      })
      await ctx.prisma.orderHistory.create({
        data: {
          orderId: order.id,
          status: order.status,
        },
      })

      pubsub.publish(ORDER_UPDATED, { orderUpdated: order })
      return order
    },
  },
}
