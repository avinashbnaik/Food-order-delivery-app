import { Context } from '../model/appInterface'

export const resolvers = {
  Mutation: {
    createMeal: async (parent, args, ctx: Context) => {
      const meal = await ctx.prisma.meal.create({
        data: {
          name: args.name,
          description: args.description,
          restaurantId: args.restaurantId,
          price: args.price,
        },
      })
      return {
        id: meal.id,
        name: meal.name,
        description: meal.description,
      }
    },
    updateMeal: async (parent, args, ctx: Context) => {
      const meal = await ctx.prisma.meal.update({
        data: {
          name: args.name,
          description: args.description,
          restaurantId: args.restaurantId,
          price: args.price,
        },
        where: {
          id: args.id,
        },
      })
      return {
        id: meal.id,
        name: meal.name,
        description: meal.description,
      }
    },
    deleteMeal: async (parent, args, ctx: Context) => {
      const meal = await ctx.prisma.meal.delete({
        where: {
          id: args.id,
        },
      })
      return {
        id: meal.id,
        name: meal.name,
        description: meal.description,
      }
    },
  },
}
