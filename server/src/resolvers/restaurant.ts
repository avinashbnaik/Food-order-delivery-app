import { Context } from '../model/appInterface'

import utils from '../utils'
export const resolvers = {
  Query: {
    hello: (parent, args, ctx: Context) => {
      return 'Restaurant'
    },
    restaurants: async (parent, args, ctx: Context) => {
      const userId = utils.getUserId(ctx)
      if (!userId) {
        throw new Error('Invalid User/Missing bearer token')
      }
      const rests = await ctx.prisma.restaurant.findMany({
        where: {
          blockedUser: {
            none: {
              userId: userId,
            },
          },
        },
      })

      return rests
    },

    restaurantById: (parent, args, ctx: Context) => {
      return ctx.prisma.restaurant.findUnique({ where: { id: args.id } })
    },
  },

  Restaurant: {
    meals: (parent, args, ctx: Context) => {
      return ctx.prisma.meal.findMany({
        where: { restaurantId: parent.id },
      })
    },

    orders: async (parent, args, ctx: Context) => {
      const ordrs = await ctx.prisma.order.findMany({
        where: { restaurantId: parent.id },
      })
      return ordrs
    },

    blockedUsers: async (parent, args, ctx: Context) => {
      const blockedUsers = await ctx.prisma.blockedUser.findMany({
        where: { restaurantId: parent.id },
      })
      return blockedUsers
    },
  },
  BlockedUser: {
    user: async (parent, args, ctx: Context) => {
      const blockedUsers = await ctx.prisma.user.findUnique({
        where: { id: parent.userId },
      })
      return blockedUsers
    },
  },
  Mutation: {
    createRestaurant: async (parent, args, ctx: Context) => {
      const { role, userId } = await utils.getUser(ctx)
      if (role !== 'RESTAURANT_OWNER') {
        throw new Error('Not an owner')
      }

      const restaurant = await ctx.prisma.restaurant.create({
        data: {
          name: args.name,
          description: args.description,
          ownerId: userId,
        },
      })
      return {
        name: restaurant.name,
        description: restaurant.description,
        id: restaurant.id,
      }
    },
    updateRestaurant: async (parent, args, ctx: Context) => {
      const isOwner = await utils.isOwner(ctx)
      if (!isOwner) {
        throw new Error('Not an owner')
      }
      const restaurant = await ctx.prisma.restaurant.update({
        data: {
          name: args.name,
          description: args.description,
        },
        where: {
          id: args.id,
        },
      })
      return {
        name: restaurant.name,
        description: restaurant.description,
      }
    },
    deleteRestaurant: async (parent, args, ctx: Context) => {
      const restaurant = await ctx.prisma.restaurant.delete({
        where: {
          id: args.id,
        },
      })
      return {
        name: restaurant.name,
        description: restaurant.description,
      }
    },
    blockUser: async (parent, args, ctx: Context) => {
      const isOwner = await utils.isOwner(ctx)
      if (!isOwner) {
        throw new Error('Not an owner')
      }
      const blockedUser = await ctx.prisma.blockedUser.create({
        data: {
          restaurantId: args.restaurantId,
          userId: args.userId,
        },
      })
      return {
        id: blockedUser.id,
      }
    },
    unblockUser: async (parent, args, ctx: Context) => {
      const isOwner = await utils.isOwner(ctx)
      if (!isOwner) {
        throw new Error('Not an owner')
      }
      const unblockedUser = await ctx.prisma.blockedUser.delete({
        where: {
          id: args.id,
        },
      })
      return {
        id: unblockedUser.id,
      }
    },
  },
}
