import { ApolloServer } from 'apollo-server'
import { resolvers } from './schema'
import { createContext } from './context'
import { makeExecutableSchema } from 'graphql-tools'
import { query } from './typeDefs/query'
import { user } from './typeDefs/user'
import { restaurant } from './typeDefs/restaurant'
import { mutation } from './typeDefs/mutation'
import { meal } from './typeDefs/meal'
import { order } from './typeDefs/order'
import { resolvers as restaurantResolver } from './resolvers/restaurant'
import { resolvers as mealResolver } from './resolvers/meal'
import { resolvers as orderResolver } from './resolvers/order'
import { mergeTypeDefs } from 'graphql-tools-merge-typedefs'

const schema = makeExecutableSchema({
  typeDefs: mergeTypeDefs([query, user, mutation, restaurant, meal, order]),
  resolvers: [resolvers, restaurantResolver, mealResolver, orderResolver],
})
//const schema2 = makeExecutableSchema({
//typeDefs: [query, restaurant],
//resolvers: restaurantResolvers,
//})

new ApolloServer({
  schema,
  context: createContext,
  subscriptions: {
    pathname: '/subscriptions',
    onConnect: (connectionParams, webSocket, context) => {
      console.log('Client connected')
    },
    onDisconnect: (webSocket, context) => {
      console.log('Client disconnected')
    },
  },
}).listen({ port: 4000 }, () =>
  console.log(`🚀 Server ready at: http://localhost:4000 ⭐️⭐️⭐️⭐️`),
)
