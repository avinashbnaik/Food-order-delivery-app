import { gql } from 'apollo-server'

export const order = gql`
  enum STATUS {
    PLACED
    CANCELLED
    PROCESSING
    INROUTE
    DELIVERED
    RECEIVED
  }
  type Order {
    id: ID!
    createdAt: String!
    updatedAt: String!
    restaurantId: ID!
    userId: ID!
    items: [OrderItemType]
    status: STATUS
    restaurant: Restaurant
    user: User
    history: [OrderHistory]
  }
  type OrderHistory {
    id: ID
    createdAt: String
    updatedAt: String
    orderId: ID
    status: STATUS
  }

  type OrderItemType {
    mealId: ID!
    orderId: ID
    createdAt: DateTime
    updatedAt: DateTime
    id: ID
    meal: Meal
    quantity: Int
  }

  input OrderItem {
    mealId: ID!
    orderId: ID
    createdAt: DateTime
    updatedAt: DateTime
    id: ID
    meal: MealInput
    quantity: Int
  }

  type OrderPayload {
    restaurantId: ID!
  }

  type OrderOut {
    id: ID!
  }

  type Mutation {
    createOrder(restaurantId: ID!, items: [OrderItem]): Order
    updateOrder(orderId: ID!, status: String): Order
  }

  type Subscription {
    orderCreated(restaurantIds: [ID]!): Order
    orderUpdated(orderId: ID!): Order
  }

  type Query {
    orders(restaurantId: ID): [Order]
    orderById(id: ID): Order
  }
`
