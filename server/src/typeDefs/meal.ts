import { gql } from 'apollo-server'

export const meal = gql`
  type Meal {
    name: String
    id: ID!
    description: String
    createdAt: String!
    updatedAt: String!
    restaurantId: ID!
    price: Float!
  }
  input MealInput {
    name: String
    id: ID!
    description: String
    createdAt: String!
    updatedAt: String!
    restaurantId: ID!
    price: Float!
  }
  type MealPayload {
    name: String
    description: String
    restaurantId: ID!
    price: Float!
  }
  type Mutation {
    createMeal(
      name: String
      description: String
      restaurantId: ID
      price: Float!
    ): MealPayload
    updateMeal(
      name: String
      description: String
      restaurantId: ID
      price: Float!
      id: ID!
    ): MealPayload
    deleteMeal(id: ID!): MealPayload
  }
  type Query {
    meals(restaurantId: ID): [Meal]
  }
`
