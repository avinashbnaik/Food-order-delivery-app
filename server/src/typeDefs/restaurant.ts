import { gql } from 'apollo-server'

export const restaurant = gql`
  type Restaurant {
    name: String!
    id: ID!
    description: String
    createdAt: String!
    updatedAt: String!
    meals: [Meal]
    orders: [Order]
    blockedUsers: [BlockedUser]
  }
  type BlockedUser {
    id: ID!
    userId: ID!
    restaurantId: ID!
    user: User
  }
  type RestaurantPayload {
    name: String
    description: String
  }
  type Mutation {
    createRestaurant(name: String, description: String, ownerId: ID): Restaurant
    updateRestaurant(
      id: ID!
      name: String
      description: String
    ): RestaurantPayload
    deleteRestaurant(id: ID!): RestaurantPayload
    blockUser(userId: ID!, restaurantId: ID!): BlockedUser
    unblockUser(id: ID!): BlockedUser
  }
  type Query {
    restaurants: [Restaurant]
    restaurantById(id: ID): Restaurant
  }
`
