export const RESTAURANT = "app/restaurant";
export const restaurant = (payload: any) => {
  return {
    type: RESTAURANT,
    payload,
  };
};
