export const MENU = "app/menu";
export const ADD_MEAL = "app/ADD_MENU";
export const UPDATE_MEAL = "app/UPDATE_MEAL";
export const DELETE_MEAL = "app/DELETE_MEAL";
export const DELETE_RESTAURANT = "app/DELETE_RESTAURANT";

export const addMeal = (payload: any) => {
  return {
    type: ADD_MEAL,
    payload,
  };
};
export const updateMeal = (payload: any) => {
  return {
    type: UPDATE_MEAL,
    payload,
  };
};

export const deleteMeal = (payload: any) => {
  return {
    type: DELETE_MEAL,
    payload,
  };
};

export const deleteRestaurant = (payload: any) => {
  return {
    type: DELETE_RESTAURANT,
    payload,
  };
};
