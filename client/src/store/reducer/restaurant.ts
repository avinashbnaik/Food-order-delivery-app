import { RESTAURANT } from "../actions/restaurant";
const INITIAL_STATE = {
  restaurants: [],
};

const reducer = (state = INITIAL_STATE, action: any) => {
  switch (action.type) {
    case RESTAURANT:
      return {
        ...state,
        restaurants: action.payload.restaurants,
      };

    default:
      return state;
  }
};

export default reducer;
