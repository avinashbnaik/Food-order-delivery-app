import {
  ADD_MEAL,
  UPDATE_MEAL,
  DELETE_MEAL,
  DELETE_RESTAURANT,
} from "../actions/menu";
const INITIAL_STATE: any = {
  orderItems: {},
};

const reducer = (state = INITIAL_STATE, action: any) => {
  switch (action.type) {
    case ADD_MEAL: {
      const { restaurantId, item: menuItem } = action.payload;
      const orderItems = state.orderItems;

      menuItem.totalPrice = menuItem.price.toFixed(2);

      if (orderItems[restaurantId]) {
        orderItems[restaurantId].push(menuItem);
      } else {
        orderItems[restaurantId] = [menuItem];
      }
      return {
        ...state,
        orderItems: { ...orderItems },
      };
    }

    case DELETE_MEAL: {
      const { restaurantId, item: menuItem } = action.payload;
      const orderItems = state.orderItems;
      const idx = orderItems[restaurantId].findIndex(
        (item: any) => item.id === menuItem.id
      );
      orderItems[restaurantId].splice(idx, 1);
      return {
        ...state,
        orderItems: { ...orderItems },
      };
    }
    case UPDATE_MEAL: {
      const { restaurantId, item: menuItem } = action.payload;
      const orderItems = state.orderItems;
      const idx = orderItems[restaurantId].findIndex(
        (item: any) => item.id === menuItem.id
      );
      const totalPrice = menuItem.price * menuItem.quantity;
      menuItem.totalPrice = totalPrice.toFixed(2);
      orderItems[restaurantId].splice(idx, 1, menuItem);
      return {
        ...state,
        orderItems: { ...orderItems },
      };
    }
    case DELETE_RESTAURANT: {
      const { restaurantId } = action.payload;
      const orderItems = state.orderItems;
      delete orderItems[restaurantId];
      return {
        ...state,
        orderItems: { ...orderItems },
      };
    }
    default:
      return state;
  }
};

export default reducer;
