import { combineReducers } from "redux";

import restaurantReducer from "./restaurant";
import menuReducer from "./menu";
const rootReducer = combineReducers({
  restaurants: restaurantReducer,
  menu: menuReducer,
});

export default rootReducer;
