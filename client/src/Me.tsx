import React from "react";
import { gql, useQuery } from "@apollo/client";
import { PostsContext } from "./Context";
export const QUERY = gql`
  query Me {
    me {
      id
      name
      email
      role
    }
  }
`;

export default function Me() {
  const { data } = useQuery(QUERY);
  const context = React.useContext(PostsContext);

  React.useEffect(() => {
    if (data?.me?.id) {
      context.updateUser(data.me);
    }
  }, [data]);

  return null;
}
