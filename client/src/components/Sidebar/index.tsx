import React, { useEffect, useState } from "react";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import clsx from "clsx";
import useStyles from "./styles";
import { useHistory, useLocation } from "react-router-dom";
import { PostsContext } from "../../Context";
import { ROLES } from "../../utils";

let ITEMS: any = [
  { name: "Restaurants", route: "/secure/restaurants" },
  { name: "Orders", route: "/secure/orders" },
];
const OWNER_ITEMS: any = [
  ...ITEMS,
  { name: "Blocked Users", route: "/secure/blocked" },
];
export default function Sidebar({ open }: any) {
  const history = useHistory();
  const classes = useStyles();
  const location = useLocation();
  const context = React.useContext(PostsContext);
  const [routes, setRoutes] = useState<any>([]);
  useEffect(() => {
    if (context?.user?.role === ROLES.RESTAURANT_OWNER) {
      setRoutes(OWNER_ITEMS);
    }
    if (context?.user?.role === ROLES.USER) {
      setRoutes(ITEMS);
    }
  }, [context]);

  return (
    <aside>
      <Drawer
        className={classes.drawer}
        variant="persistent"
        anchor="left"
        open={open}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <List>
          {routes.map((item: any) => (
            <>
              <ListItem
                button
                key={item.route}
                className={clsx(
                  classes.listItem,
                  location.pathname.includes(item.route)
                    ? classes.activeListItem
                    : ""
                )}
                onClick={() => history.push(item.route)}
              >
                {item.name}
              </ListItem>
            </>
          ))}
        </List>
      </Drawer>
    </aside>
  );
}
