import { makeStyles } from "@material-ui/core/styles";

const drawerWidth = 150;
const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  hide: {
    display: "none",
  },
  drawer: {
    marginTop: 70,
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    marginTop: 80,
    paddingTop: 170,
    width: drawerWidth,
  },
  drawerHeader: {
    display: "flex",
    alignItems: "center",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
    justifyContent: "flex-end",
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },

  listItem: {
    paddingLeft: theme.spacing(3),
    // padding: theme.spacing(1.2),
    padding: "10px 16px 10px 24px",
    borderTopRightRadius: theme.spacing(2),
    borderBottomRightRadius: theme.spacing(2),
    cursor: "pointer",
    color: "#5f6368",
    "&:hover": {
      backgroundColor: theme.palette.grey[300],
    },
    transition: theme.transitions.create(["background-color", "transform"]),
    display: "flex",
    outline: 0,
    fontSize: "0.875rem",
    lineHeight: 1.43,
    letterSpacing: "0.01071em",
    justifyContent: "flex-start",
    fontWeight: 500,
  },
  menu: {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    position: "fixed",
    zIndex: 100,
  },
  activeListItem: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.common.white,
    fontWeight: "bold",
    "&:hover": {
      backgroundColor: theme.palette.primary.main,
      color: theme.palette.common.white,
    },
    "&:focus": {
      backgroundColor: theme.palette.primary.main,
      color: theme.palette.common.white,
    },
  },
}));

export default useStyles;
