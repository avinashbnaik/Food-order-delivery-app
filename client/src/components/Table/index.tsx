import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles({
  table: {},
  cancel: {
    backgroundColor: "tomato",
  },
});

export default function SpanningTable({
  header,
  onSubmit,
  onCancel,
  items = [],
}: any) {
  const classes = useStyles();
  const total =
    items.reduce((a: number, c: any) => {
      return a + parseFloat(c.totalPrice);
    }, 0) || 0;
  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="spanning table">
        <TableHead>
          <TableRow>
            <TableCell align="center" colSpan={3}>
              <h4>Order Details</h4>
              {header ? header : null}
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell>Meal Name</TableCell>
            <TableCell align="right">Qty.</TableCell>
            <TableCell align="right">Price</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {items.map((row: any) => (
            <TableRow key={row.name}>
              <TableCell>{row.name}</TableCell>
              <TableCell align="right">{row.quantity}</TableCell>
              <TableCell align="right">
                ${parseFloat(row.totalPrice).toFixed(2)}
              </TableCell>
            </TableRow>
          ))}

          <TableRow>
            <TableCell colSpan={2}>Total</TableCell>
            <TableCell align="right">${parseFloat(total).toFixed(2)}</TableCell>
          </TableRow>
        </TableBody>
      </Table>
      {onSubmit ? (
        <Button
          fullWidth
          onClick={onSubmit}
          variant="contained"
          color="primary"
        >
          Order
        </Button>
      ) : null}
      {onCancel ? (
        <Button
          fullWidth
          onClick={onCancel}
          variant="contained"
          className={classes.cancel}
        >
          Cancel
        </Button>
      ) : null}
    </TableContainer>
  );
}
