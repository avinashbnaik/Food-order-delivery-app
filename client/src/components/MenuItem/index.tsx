import React, { useState } from "react";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import { Tooltip, Typography, IconButton, Button } from "@material-ui/core";
import { AddCircleTwoTone, RemoveCircleTwoTone } from "@material-ui/icons";
interface IProps {
  id: string;
  name: string;
  price: number;
  quantity: number;
  description: string;
  onAdd: (item: any) => void;
  onUpdate: (item: any) => void;
  onRemove: (item: any) => void;
}
const MenuItem = ({
  id,
  name,
  description,
  price,
  onAdd,
  onUpdate,
  onRemove,
}: IProps) => {
  const [quantity, setQuantity] = useState<number>(0);
  const onDecrement = () => {
    if (quantity === 1) {
      onRemove({ id, name, quantity: quantity - 1, price });
    } else {
      onUpdate({ id, name, quantity: quantity - 1, price });
    }

    setQuantity((q) => q - 1);
  };
  return (
    <Paper
      style={{ flexGrow: 1, padding: "16px 16px", margin: "8px 0px" }}
      elevation={2}
    >
      <Grid container>
        <Grid item xs={10} spacing={2}>
          <Grid xs={12} spacing={2}>
            <Typography variant="h6" component="h6">
              {name}
            </Typography>
          </Grid>
          <Grid item xs={12} spacing={2}>
            <Tooltip title={description}>
              <Typography
                variant="subtitle1"
                component="h6"
                className="gray overflow-elipsis"
              >
                {description}
              </Typography>
            </Tooltip>
          </Grid>
          <Grid item xs={12} spacing={2}>
            <Grid item xs={6} spacing={2}>
              ${price}
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={2} spacing={2}>
          <Grid container justify="center" alignItems="center">
            {quantity === 0 ? (
              <Button
                onClick={() => {
                  onAdd({ name, id, quantity: quantity + 1, price });
                  setQuantity((p) => p + 1);
                }}
                variant="contained"
                color="primary"
              >
                Add
              </Button>
            ) : (
              <>
                <IconButton
                  onClick={() => {
                    onUpdate({ name, id, quantity: quantity + 1, price });
                    setQuantity((p) => p + 1);
                  }}
                  color="primary"
                >
                  <AddCircleTwoTone />
                </IconButton>
                <Typography variant="subtitle1" component="h6">
                  {quantity}
                </Typography>
                <IconButton onClick={onDecrement} color="primary">
                  <RemoveCircleTwoTone />
                </IconButton>
              </>
            )}
          </Grid>
        </Grid>
      </Grid>
    </Paper>
  );
};

export default MenuItem;
