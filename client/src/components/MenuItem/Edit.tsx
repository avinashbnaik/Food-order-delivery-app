import React from "react";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import { Tooltip, Typography, IconButton } from "@material-ui/core";
import { EditTwoTone, DeleteTwoTone } from "@material-ui/icons";
interface IProps {
  id: string;
  name: string;
  price: number;
  quantity: number;
  description: string;
  onEdit: (item: any) => void;
  onDelete: (item: any) => void;
}
const MenuItem = ({
  id,
  onDelete,
  name,
  description,
  price,
  onEdit,
}: IProps) => {
  return (
    <Paper
      style={{ flexGrow: 1, padding: "16px 16px", margin: "8px 0px" }}
      elevation={2}
    >
      <Grid container>
        <Grid item xs={10} spacing={2}>
          <Grid xs={12} spacing={2}>
            <Typography variant="h6" component="h6">
              {name}
            </Typography>
          </Grid>
          <Grid item xs={12} spacing={2}>
            <Tooltip title={description}>
              <Typography
                variant="subtitle1"
                component="h6"
                className="gray overflow-elipsis"
              >
                {description}
              </Typography>
            </Tooltip>
          </Grid>
          <Grid item xs={12} spacing={2}>
            <Grid item xs={6} spacing={2}>
              ${price}
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={2} spacing={2}>
          <Grid container justify="center" alignItems="center">
            <IconButton
              onClick={() => onEdit({ id, name, description, price })}
            >
              <EditTwoTone />
            </IconButton>
            <IconButton onClick={() => onDelete(id)}>
              <DeleteTwoTone />
            </IconButton>
          </Grid>
        </Grid>
      </Grid>
    </Paper>
  );
};

export default MenuItem;
