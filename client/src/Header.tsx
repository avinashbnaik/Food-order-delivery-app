import { AppBar, Toolbar, Typography, Button } from "@material-ui/core";
import React from "react";
import { Link } from "react-router-dom";
import { PostsContext } from "./Context";
import Logout from "./pages/auth/Logout";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    githubButton: {
      color: "black",
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      color: "white",
      flexGrow: 1,
    },
  })
);

export default function Header() {
  const context = React.useContext(PostsContext);
  const classes = useStyles();
  return (
    <div>
      <div className={classes.root}>
        <AppBar position="fixed">
          <Toolbar>
            <Typography variant="h6" className={classes.title}>
              <Link className={classes.title} to="/">
                TopFood
              </Link>
            </Typography>
            {context.user.id ? (
              <>
                <Logout />
              </>
            ) : (
              <>
                <Link to="/signup">
                  <Button variant="contained" color="primary">
                    Signup
                  </Button>
                </Link>{" "}
                <Link to="/login">
                  <Button variant="contained" color="secondary">
                    Login
                  </Button>
                </Link>
              </>
            )}
          </Toolbar>
        </AppBar>
      </div>
    </div>
  );
}
