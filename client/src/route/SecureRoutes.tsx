import React from "react";
import { Switch, Redirect } from "react-router-dom";
import Users from "../pages/user/Users";
import PrivateRoute from "./PrivateRoute";
import User from "../pages/user/UserPage";
import Orders from "../pages/order/Orders";
import Order from "../pages/order/Order";
import RestaurantMenu from "../pages/restaurant/RestaurantMenu";
import MealManager from "../pages/restaurant/meal/MealManager";
import Restaurants from "../pages/restaurant/Restaurants";
import BlockedUsers from "../pages/user/BlockedUsers";

import Sidebar from "../components/Sidebar";
import { PostsContext } from "../Context";
import { ROLES } from "../utils";

export default function RouteApp() {
  const context = React.useContext(PostsContext);

  return (
    <>
      <Sidebar open />
      <Redirect from="/secure" to="/secure/restaurants" />
      <div style={{ paddingLeft: 170, paddingRight: 20 }}>
        <Switch>
          <PrivateRoute path="/secure/restaurants" component={Restaurants} />
          <PrivateRoute path="/secure/users" component={Users} />
          <PrivateRoute path="/secure/user/:userId" component={User} />
          <PrivateRoute
            path="/secure/restaurant/:id"
            component={
              context?.user?.role === ROLES.RESTAURANT_OWNER
                ? MealManager
                : RestaurantMenu
            }
          />
          {context?.user?.role === ROLES.RESTAURANT_OWNER && (
            <PrivateRoute path="/secure/blocked" component={BlockedUsers} />
          )}

          <PrivateRoute path="/secure/orders" component={() => <Orders />} />
          <PrivateRoute path="/secure/order/:id" component={Order} />
        </Switch>
      </div>
    </>
  );
}
