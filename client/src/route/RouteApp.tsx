import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import Signup from "../pages/auth/Signup";
import Login from "../pages/auth/Login";
import ForgetPassword from "../pages/auth/ForgetPassword";
import ResetPassword from "../pages/auth/ResetPassword";
import PublicRoute from "./PublicRoute";
import SecureRoutes from './SecureRoutes'

export default function RouteApp() {
  return (
    <Switch>
      <Route exact path="/"  >
          <Redirect to="/secure" />
      </Route>

      <PublicRoute path="/signup" component={Signup} />
      <PublicRoute path="/login" component={Login} />
      <PublicRoute path="/forgetPassword" component={ForgetPassword} />
      <PublicRoute
        path="/resetPassword/:resetPasswordToken"
        component={ResetPassword}
      />

      <Route path="/secure" component={SecureRoutes} />

    </Switch>
  );
}
