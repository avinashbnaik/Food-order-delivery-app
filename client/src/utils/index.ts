export const getDateTime = (secs: string) => {
  const today = new Date(parseInt(secs, 10));

  const stamp = today.toString().split("GMT")[0];
  return `${stamp}`;
};

export const ORDERSTAGES = [
  "PLACED",
  "PROCESSING",
  "INROUTE",
  "DELIVERED",
  "RECEIVED",
];

export const STAGE_MAP: any = {
  PLACED: "PROCESSING",
  PROCESSING: "INROUTE",
  INROUTE: "DELIVERED",
  DELIVERED: "RECEIVED",
};
export const getNextStage = (stage: any): any => {
  return STAGE_MAP[stage];
};

export const ROLES = {
  USER: "USER",
  RESTAURANT_OWNER: "RESTAURANT_OWNER",
};
