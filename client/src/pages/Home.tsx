import React from "react";
import { useHistory } from "react-router-dom";
import { PostsContext } from "../Context";
import { Link } from "react-router-dom";
import Restaurants from "./restaurant/Restaurants";

const Home = () => {
  const history = useHistory();
  const context = React.useContext(PostsContext);
  React.useEffect(() => {
    if (!context.user.id) {
      history.push("/login");
    }
  }, [context]);

  return (
    <>
      <h3>Home</h3>
      Hello {context.user.name}. Your email is {context.user.email}
      <div>
          <Link to={`/secure/user/${context.user.id}`}>My profile</Link>
        <Restaurants />
      </div>
    </>
  );
};

export default Home;
