import React, { useEffect, useState } from "react";
import {
  gql,
  useSubscription,
  useMutation,
  useLazyQuery,
} from "@apollo/client";
import Stepper from "../../components/Stepper";
import { PostsContext } from "../../Context";
import { ROLES, STAGE_MAP, getDateTime } from "../../utils";

// import CircularProgress from "@material-ui/core/CircularProgress";
import Table from "../../components/Table";
import {
  IconButton,
  CircularProgress,
  Paper,
  Typography,
  Tooltip,
} from "@material-ui/core";
import { ToastContainer, toast } from "react-toastify";
import { BlockTwoTone } from "@material-ui/icons";

import "react-toastify/dist/ReactToastify.css";

const ORDER_SUBSCRIPTION = gql`
  subscription ($orderId: ID!) {
    orderUpdated(orderId: $orderId) {
      id
      status
      updatedAt
      createdAt
    }
  }
`;
const UPDATE_ORDER = gql`
  mutation updateOrder($orderId: ID!, $status: String) {
    updateOrder(orderId: $orderId, status: $status) {
      id
      status
    }
  }
`;

export const BLOCK_QUERY = gql`
  mutation blockUser($restaurantId: ID!, $userId: ID!) {
    blockUser(restaurantId: $restaurantId, userId: $userId) {
      id
    }
  }
`;
export const UNBLOCK_QUERY = gql`
  mutation unblockUser($id: ID!) {
    unblockUser(id: $id) {
      id
    }
  }
`;

const ORDER_HISTORY = gql`
  query orderById($id: ID) {
    orderById(id: $id) {
      id
      userId
      createdAt
      updatedAt
      status
      restaurant {
        name
        id
        blockedUsers {
          userId
          id
        }
      }
      user {
        name
        id
      }
      items {
        quantity
        meal {
          name
          description
          price
        }
      }
      history {
        createdAt
        updatedAt
        status
        id
      }
    }
  }
`;

// interface MatchParams {
//   id: string;
// }

// interface IProps extends RouteComponentProps<MatchParams> {}
const Order = ({ match: { params: { id = "" } = {} } = {} }: any) => {
  const { data = {} } = useSubscription(ORDER_SUBSCRIPTION, {
    variables: { orderId: id },
  });
  const [blockUser] = useMutation(BLOCK_QUERY);

  const [unblockUser] = useMutation(UNBLOCK_QUERY);
  const [isBlocked, setIsBlocked] = useState<boolean>(false);
  const [blockedId, setBlockedId] = useState<string>("");

  const [updateOrder] = useMutation(UPDATE_ORDER);
  const context = React.useContext(PostsContext);

  const [fetchOrder, { loading, data: response = {} }] = useLazyQuery(
    ORDER_HISTORY,
    { fetchPolicy: "network-only" }
  );
  const { orderById = {} } = response;

  const { orderUpdated: { status = "" } = {} }: any = data;
  const orderItems = orderById?.items?.map((obj: any) => ({
    ...obj.meal,
    quantity: obj.quantity,
    totalPrice: parseFloat(obj?.meal?.price) * parseFloat(obj.quantity),
  }));
  useEffect(() => {
    const isBlocked = orderById?.restaurant?.blockedUsers.filter(
      (user: any) => user.userId === orderById?.user?.id
    );
    if (isBlocked?.length > 0) {
      setIsBlocked(isBlocked?.length > 0);
      setBlockedId(isBlocked[0]?.id);
    } else {
      setIsBlocked(false);
    }
  }, [orderById?.restaurant?.blockedUsers, orderById?.user?.id]);
  const onBlockUser = async () => {
    try {
      const { errors = [] } = await blockUser({
        variables: {
          restaurantId: orderById?.restaurant?.id,
          userId: orderById?.user?.id,
        },
      });

      if (errors.length > 0) {
        toast.error("Failed to block user", {
          position: toast.POSITION.TOP_RIGHT,
        });
      } else {
        toast.success("User blocked successfully!", {
          position: toast.POSITION.TOP_RIGHT,
        });
        fetchOrder({ variables: { id } });
      }
    } catch (err) {
      err.graphQLErrors.some((graphQLError: any) =>
        toast.error(graphQLError.message, {
          position: toast.POSITION.TOP_RIGHT,
        })
      );
    }
  };
  const onUnBlockUser = async () => {
    try {
      const { errors = [] } = await unblockUser({
        variables: {
          id: blockedId,
        },
      });

      if (errors.length > 0) {
        toast.error("Failed to unblock user", {
          position: toast.POSITION.TOP_RIGHT,
        });
      } else {
        toast.success("User unblocked successfully!", {
          position: toast.POSITION.TOP_RIGHT,
        });
        fetchOrder({ variables: { id } });
      }
    } catch (err) {
      err.graphQLErrors.some((graphQLError: any) =>
        toast.error(graphQLError.message, {
          position: toast.POSITION.TOP_RIGHT,
        })
      );
    }
  };
  useEffect(() => {
    fetchOrder({ variables: { id } });
  }, [status]);

  const cancelOrder = async () => {
    try {
      const { errors = [] } = await updateOrder({
        variables: {
          orderId: id,
          status: "CANCELLED",
        },
      });
      if (errors.length > 0) {
        toast.error("Failed to cancel order", {
          position: toast.POSITION.TOP_RIGHT,
        });
      } else {
        toast.success("Order cancelled successfully!", {
          position: toast.POSITION.TOP_RIGHT,
        });
      }
    } catch (err) {
      err.graphQLErrors.some((graphQLError: any) =>
        toast.error(graphQLError.message, {
          position: toast.POSITION.TOP_RIGHT,
        })
      );
    }
  };

  const onNext = async () => {
    const nextStage = STAGE_MAP[orderById?.status];
    if (nextStage) {
      try {
        const { errors = [] } = await updateOrder({
          variables: {
            orderId: id,
            status: nextStage,
          },
        });
        if (errors.length > 0) {
          toast.error("Failed to update order", {
            position: toast.POSITION.TOP_RIGHT,
          });
        } else {
          toast.success("Order status updated successfully!", {
            position: toast.POSITION.TOP_RIGHT,
          });
        }
      } catch (err) {
        err.graphQLErrors.some((graphQLError: any) =>
          toast.error(graphQLError.message, {
            position: toast.POSITION.TOP_RIGHT,
          })
        );
      }
    }
  };

  const sortedHistory = orderById?.history
    ?.slice()
    .sort((a: any, b: any) => b.updatedAt - a.updatedAt);
  if (loading) {
    return <CircularProgress color="primary" />;
  }
  return (
    <div className="container">
      <ToastContainer />

      <Typography component="h6" variant="subtitle1" className="gray center">
        Order ID: #{orderById.id}
      </Typography>
      <Typography component="h6" variant="h5" className="center">
        Status: {orderById?.status}
      </Typography>
      <Stepper
        onNext={
          ((context?.user?.role === ROLES.RESTAURANT_OWNER &&
            orderById?.status !== "DELIVERED") ||
            (orderById?.status === "DELIVERED" &&
              context?.user?.role === ROLES.USER)) &&
          onNext
        }
        currentStage={
          orderById?.status === "CANCELLED"
            ? sortedHistory[1]?.status
            : orderById?.status
        }
        isCancelled={orderById?.status === "CANCELLED"}
      />
      <Table
        header={
          <div>
            {`Restaurant: ${orderById?.restaurant?.name}`}
            {context?.user?.role === ROLES.RESTAURANT_OWNER && (
              <div>
                {`User: ${orderById?.user?.name}
        `}
                {isBlocked ? (
                  <Tooltip title="Unblock user">
                    <IconButton
                      onClick={onUnBlockUser}
                      style={{ color: "red" }}
                    >
                      <BlockTwoTone />
                    </IconButton>
                  </Tooltip>
                ) : (
                  <Tooltip title="Block user">
                    <IconButton onClick={onBlockUser}>
                      <BlockTwoTone />
                    </IconButton>
                  </Tooltip>
                )}
              </div>
            )}
          </div>
        }
        items={orderItems}
        onCancel={
          context?.user?.role !== ROLES.RESTAURANT_OWNER &&
          !["RECEIVED", "DELIVERED", "CANCELLED"].includes(orderById?.status) &&
          cancelOrder
        }
      />
      <div style={{ padding: "20px 0px" }}>
        {sortedHistory?.length > 0 && (
          <Typography
            style={{ paddingLeft: 8 }}
            className="center"
            component="h5"
            variant="h6"
          >
            Order History
          </Typography>
        )}
        {sortedHistory?.map((hist: any) => (
          <Paper
            style={{
              padding: "4px 16px",
              borderBottom: "1px solid rgba(255,255,255,0.3)",
              margin: 4,
            }}
            key={hist.id}
          >
            <Typography>Status: {hist.status}</Typography>
            <Typography className="gray" style={{ fontSize: 13 }}>
              updatedAt: {getDateTime(hist?.updatedAt)}
            </Typography>
          </Paper>
        ))}
      </div>
    </div>
  );
};

export default Order;
