import React, { useState, useEffect } from "react";
import { gql, useLazyQuery } from "@apollo/client";
import Card from "../../components/Card";
import { Grid, Typography } from "@material-ui/core/";
import { useHistory } from "react-router-dom";
import "./index.css";
import { getDateTime } from "../../utils";
import { ROLES } from "../../utils";
import { PostsContext } from "../../Context";

export const QUERY = gql`
  {
    me {
      orders {
        createdAt
        updatedAt
        status
        id
        restaurant {
          name
        }
        items {
          meal {
            name
            description
          }
        }
      }
    }
  }
`;
const ORDER_CREATE_SUBSCRIPTION = gql`
  subscription ($restaurantIds: [ID]!) {
    orderCreated(restaurantIds: $restaurantIds) {
      id
      status
      updatedAt
      createdAt
    }
  }
`;

export const OWNER_QUERY = gql`
  {
    me {
      email
      restaurants {
        name
        id
        orders {
          id
          createdAt
          updatedAt
          status
          userId
          restaurant {
            name
            id
          }
          user {
            name
          }
          items {
            meal {
              name
              description
            }
          }
          history {
            status
            createdAt
            updatedAt
            orderId
          }
        }
      }
    }
  }
`;
const Orders = () => {
  const [fetchOrders, { data: response = {} }] = useLazyQuery(QUERY);
  const [fetchOwnerOrders, { subscribeToMore, data: ownerResponse = {} }] =
    useLazyQuery(OWNER_QUERY, {
      fetchPolicy: "network-only",
    });

  const context = React.useContext(PostsContext);

  const [orderItems, setOrderItems] = useState<any>([]);
  const { me: { restaurants = [] } = {} } = ownerResponse;
  // const { data = {} } = useSubscription(ORDER_CREATE_SUBSCRIPTION, {
  //   variables: { restaurantIds: restaurants?.map((r: any) => r.id) },
  // });

  useEffect(() => {
    if (
      context?.user?.role === ROLES.RESTAURANT_OWNER &&
      restaurants?.length > 0 &&
      subscribeToMore
    ) {
      subscribeToMore({
        document: ORDER_CREATE_SUBSCRIPTION,
        variables: { restaurantIds: restaurants?.map((r: any) => r.id) },
        updateQuery: (prev) => {
          // if (!subscriptionData.data) return prev;
          fetchOwnerOrders();
          return prev;
        },
      });
    }
  }, [restaurants]);
  // useEffect(() => {
  //   if (context?.user?.role === ROLES.RESTAURANT_OWNER) {
  //     fetchOwnerOrders();
  //   }
  // }, []);
  useEffect(() => {
    if (context?.user?.role === ROLES.RESTAURANT_OWNER) {
      fetchOwnerOrders();
    } else {
      fetchOrders();
    }
  }, [context?.user?.role]);
  useEffect(() => {
    if (context?.user?.role === ROLES.USER) {
      const { me: { orders = [] } = {} } = response;
      setOrderItems(orders);
    }
  }, [response]);

  useEffect(() => {
    if (context?.user?.role === ROLES.RESTAURANT_OWNER) {
      const { me: { restaurants = [] } = {} } = ownerResponse;
      let allOrders: any = [];
      restaurants.forEach(({ orders = [] }) => {
        allOrders = allOrders.concat(orders);
      });

      setOrderItems(allOrders);
    }
  }, [ownerResponse]);
  const history = useHistory();

  return (
    <div className="container">
      <h3>Orders</h3>
      <Grid container spacing={2}>
        {orderItems?.length ? (
          orderItems
            ?.slice()
            ?.reverse()
            ?.map((order: any) => (
              <Grid key={order.id} item xs={12} sm={6}>
                <Card
                  title={
                    <div>
                      {order?.restaurant?.name}
                      <div
                        style={{
                          fontSize: 12,
                          padding: "4px 0px",
                          color: "gray",
                        }}
                      >
                        OrderId: {order?.id}
                      </div>
                    </div>
                  }
                  description={
                    <div>
                      {`${
                        context?.user?.role === ROLES.RESTAURANT_OWNER
                          ? order?.user?.name
                          : "You"
                      } Ordered ${order.items?.length} ${
                        order.items?.length === 1 ? "item" : "items"
                      }`}
                      <div className="gray">
                        on {getDateTime(order.createdAt)}
                      </div>
                    </div>
                  }
                  onClick={() => history.push(`/secure/order/${order.id}`)}
                />
              </Grid>
            ))
        ) : (
          <Typography className="gray">No Orders found</Typography>
        )}
      </Grid>
    </div>
  );
};
export default Orders;
