import React, { useEffect, useState } from "react";
import { gql, useLazyQuery, useMutation } from "@apollo/client";
import { Typography } from "@material-ui/core";
import { ToastContainer, toast } from "react-toastify";
import UserTable from "../../components/Table/Users";

import "react-toastify/dist/ReactToastify.css";

const BLOCKED_QUERY = gql`
  {
    me {
      restaurants {
        name
        id
        blockedUsers {
          id
          user {
            name
            id
          }
        }
      }
    }
  }
`;

export const UNBLOCK_QUERY = gql`
  mutation unblockUser($id: ID!) {
    unblockUser(id: $id) {
      id
    }
  }
`;

const BlockedUsers = () => {
  const [users, setUsers] = useState<any>([]);

  const [fetchBlockedUsers] = useLazyQuery(BLOCKED_QUERY, {
    fetchPolicy: "network-only",
    onCompleted: (dt) => {
      const { me: { restaurants = [] } = {} } = dt;

      let blockedUsers: any = [];
      restaurants.forEach(({ blockedUsers: bUsers = [] }) => {
        blockedUsers = blockedUsers.concat(...bUsers);
      });

      if (blockedUsers.length > 0) {
        setUsers(blockedUsers);
      } else {
        setUsers([]);
      }
    },
  });
  const [unblockUser] = useMutation(UNBLOCK_QUERY);
  useEffect(() => {
    fetchBlockedUsers();
  }, []);
  const onUnBlockUser = async (blockedId: any) => {
    try {
      const { errors = [] } = await unblockUser({
        variables: {
          id: blockedId,
        },
      });

      if (errors.length > 0) {
        toast.error("Failed to unblock user", {
          position: toast.POSITION.TOP_RIGHT,
        });
      } else {
        toast.success("User unblocked successfully!", {
          position: toast.POSITION.TOP_RIGHT,
        });
        fetchBlockedUsers();
      }
    } catch (err) {
      err.graphQLErrors.some((graphQLError: any) =>
        toast.error(graphQLError.message, {
          position: toast.POSITION.TOP_RIGHT,
        })
      );
    }
  };
  return (
    <div className="container center">
      <ToastContainer />

      <Typography style={{ paddingTop: 20 }} component="h6" variant="h6">
        Blocked Users
      </Typography>
      <div className="section" style={{ paddingTop: 20 }}>
        {users.length === 0 ? (
          <Typography
            className="gray"
            style={{ fontSize: 15 }}
            component="h6"
            variant="h6"
          >
            You have not blocked any users
          </Typography>
        ) : (
          <UserTable items={users} onUnblock={onUnBlockUser} />
        )}
      </div>
    </div>
  );
};

export default BlockedUsers;
