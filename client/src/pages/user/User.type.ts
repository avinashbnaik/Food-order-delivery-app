export interface User {
  id: string;
  name: string;
  email: string;
  lastLogin?: Date;
  role: "ADMIN" | "USER" | "RESTAURANT_OWNER";
}

export const userClass: User = {
  id: "",
  name: "",
  role: "USER",
  email: "",
};
