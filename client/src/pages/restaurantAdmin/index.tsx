import React from "react";
import { gql, useQuery } from "@apollo/client";
import RestaurantCard from "../restaurant/RestaurantCard";
import { Restaurant } from "../restaurant/types";
import { Grid } from "@material-ui/core/";
import { useHistory } from "react-router-dom";

export const QUERY = gql`
  {
    restaurants {
      name
      description
      createdAt
      updatedAt
      id
      meals {
        name
        price
        description
      }
    }
  }
`;

const Restaurants = () => {
  const { data: { restaurants = [] } = {} } = useQuery(QUERY);
  const history = useHistory();

  return (
    <>
      <h3>Restaurants</h3>
      <Grid container spacing={2}>
        {restaurants.map((restaurant: Restaurant) => (
          <Grid key={restaurant.id} item xs={12} sm={2}>
            <RestaurantCard
              title={restaurant.name}
              description={restaurant.description}
              onClick={() => history.push(`/secure/restaurant/${restaurant.id}`)}
            />
          </Grid>
        ))}
      </Grid>
    </>
  );
};
export default Restaurants;
