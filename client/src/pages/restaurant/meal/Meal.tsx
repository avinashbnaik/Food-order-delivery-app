import React from "react";
import { gql } from "@apollo/client";
import { MenuItem } from "../../../components";
import { useDispatch } from "react-redux";
import * as actions from "../../../store/actions/menu";
export const QUERY = gql`
  {
    restaurants {
      name
      description
      createdAt
      updatedAt
      id
      meals {
        name
        price
        description
      }
    }
  }
`;
interface IProps {
  item: any;
  restaurantId: string;
}

const Meal = ({ item, restaurantId }: IProps) => {
  const dispatch = useDispatch();
  const onAdd = (item: any) => {
    dispatch(actions.addMeal({ restaurantId: restaurantId, item }));
  };
  const onUpdate = (item: any) => {
    dispatch(actions.updateMeal({ restaurantId: restaurantId, item }));
  };
  const onRemove = (item: any) => {
    dispatch(actions.deleteMeal({ restaurantId: restaurantId, item }));
  };
  return (
    <MenuItem
      onRemove={onRemove}
      onUpdate={onUpdate}
      onAdd={onAdd}
      id={item.id}
      name={item.name}
      description={item.description}
      price={item.price}
      quantity={item.quantity}
    />
  );
};
export default Meal;
