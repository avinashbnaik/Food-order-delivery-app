import React, { useEffect, useState } from "react";
import { gql, useLazyQuery, useMutation } from "@apollo/client";
import "../index.css";
import MealEdit from "./MealEdit";
import Grid from "@material-ui/core/Grid";
import { ToastContainer, toast } from "react-toastify";
import TextField from "@material-ui/core/TextField";
import Tooltip from "@material-ui/core/Tooltip";
import { useHistory } from "react-router-dom";

import { makeStyles } from "@material-ui/core/styles";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import EditIcon from "@material-ui/icons/Edit";
import RemoveIcon from "@material-ui/icons/Delete";

import "react-toastify/dist/ReactToastify.css";
import { Button, Typography } from "@material-ui/core";
export const QUERY = gql`
  query restaurants($id: ID) {
    restaurantById(id: $id) {
      name
      description
      createdAt
      updatedAt
      id
      meals {
        id
        name
        price
        description
      }
    }
  }
`;

export const CREATE_MEAL_MUTATION = gql`
  mutation createMeal(
    $name: String
    $description: String
    $restaurantId: ID!
    $price: Float!
  ) {
    createMeal(
      name: $name
      description: $description
      restaurantId: $restaurantId
      price: $price
    ) {
      name
      description
    }
  }
`;

export const UPDATE_RESTAURANT = gql`
  mutation updateRestaurant($id: ID!, $name: String, $description: String) {
    updateRestaurant(id: $id, name: $name, description: $description) {
      name
      description
    }
  }
`;

export const DELETE_RESTAURANT = gql`
  mutation deleteRestaurant($id: ID!) {
    deleteRestaurant(id: $id) {
      name
    }
  }
`;
export const UPDATE_MEAL_MUTATION = gql`
  mutation updateMeal(
    $id: ID!
    $name: String
    $description: String
    $restaurantId: ID!
    $price: Float!
  ) {
    updateMeal(
      id: $id
      name: $name
      description: $description
      restaurantId: $restaurantId
      price: $price
    ) {
      name
      description
    }
  }
`;
export const DELETE_MEAL_MUTATION = gql`
  mutation deleteMeal($id: ID!) {
    deleteMeal(id: $id) {
      name
      description
    }
  }
`;

const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
    },
  },
  section: {
    position: "relative",
  },
  deleteIcon: {
    position: "absolute",
    right: 0,
    top: 20,
  },
  editIcon: {
    position: "absolute",
    top: 20,
    right: 80,
  },
  row: {
    display: "flex",
    justifyContent: "space-around",
    alignItems: "center",
    padding: 8,
    gap: 8,
  },
}));

const MealManager = (props: any) => {
  const classes = useStyles();
  // const {
  //   data: {
  //     restaurantById: { name = "", description = "", meals = [], id = "" } = {},
  //   } = {},
  // } = useQuery(QUERY, { variables: { id: props.match.params.id } });

  const [fetchMeals, { data = {} }] = useLazyQuery(QUERY, {
    variables: { id: props.match.params.id },
    fetchPolicy: "network-only",
  });
  const {
    restaurantById: { name = "", description = "", meals = [], id = "" } = {},
  } = data;

  const [edit, setEdit] = useState<any>(false);
  const [editRestaurant, setEditRestaurant] = useState<any>(false);

  const history = useHistory();
  const [createMeal] = useMutation(CREATE_MEAL_MUTATION);
  const [updateMeal] = useMutation(UPDATE_MEAL_MUTATION);

  const [mealName, setMealName] = useState("");
  const [mealDesc, setMealDesc] = useState("");
  const [mealPrice, setMealPrice] = useState("");

  const [restaurantName, setRestaurantName] = useState("");
  const [restaurantDesc, setRestaurantDesc] = useState("");
  const [deleteMeal] = useMutation(DELETE_MEAL_MUTATION);
  const [updateRestaurant] = useMutation(UPDATE_RESTAURANT);
  const [deleteRestaurant] = useMutation(DELETE_RESTAURANT);
  useEffect(() => {
    fetchMeals();
  }, []);

  const onCreateMeal = async () => {
    const { errors = [] } = await createMeal({
      variables: {
        name: mealName,
        description: mealDesc,
        price: parseFloat(mealPrice),
        restaurantId: id,
      },
    });

    if (errors.length > 0) {
      toast.error("Failed to create meal", {
        position: toast.POSITION.TOP_RIGHT,
      });
    } else {
      toast.success("Meal created successfully!", {
        position: toast.POSITION.TOP_RIGHT,
      });
      fetchMeals();
      setEdit({});
    }
  };
  const onDeleteMeal = async (mealId: any) => {
    try {
      const { errors = [] } = await deleteMeal({
        variables: {
          id: mealId,
        },
      });

      if (errors.length > 0) {
        toast.error("Failed to delete meal", {
          position: toast.POSITION.TOP_RIGHT,
        });
      } else {
        toast.success("Meal deleted successfully!", {
          position: toast.POSITION.TOP_RIGHT,
        });
        fetchMeals();
      }
    } catch (e) {
      e.graphQLErrors.some((graphQLError: any) =>
        toast.error(graphQLError.message, {
          position: toast.POSITION.TOP_RIGHT,
        })
      );
    }
  };

  const onUpdateMeal = async () => {
    const { errors = [] } = await updateMeal({
      variables: {
        id: edit.id,
        name: mealName,
        description: mealDesc,
        price: parseFloat(mealPrice),
        restaurantId: id,
      },
    });

    if (errors.length > 0) {
      toast.error("Failed to update meal", {
        position: toast.POSITION.TOP_RIGHT,
      });
    } else {
      toast.success("Meal update successfully!", {
        position: toast.POSITION.TOP_RIGHT,
      });
      fetchMeals();
      setEdit({});
    }
  };
  const onUpdateRestaurant = async () => {
    const { errors = [] } = await updateRestaurant({
      variables: {
        id: id,
        name: restaurantName,
        description: restaurantDesc,
      },
    });

    if (errors.length > 0) {
      toast.error("Failed to update restaurant", {
        position: toast.POSITION.TOP_RIGHT,
      });
    } else {
      toast.success("Restaurant details updated successfully!", {
        position: toast.POSITION.TOP_RIGHT,
      });
      fetchMeals();
      setEditRestaurant(false);
    }
  };
  const onEditRestaurant = () => {
    setEditRestaurant(true);
    setRestaurantName(name);
    setRestaurantDesc(description);
  };
  const onRemoveRestaurant = async () => {
    const { errors = [] } = await deleteRestaurant({
      variables: {
        id: id,
      },
    });

    if (errors.length > 0) {
      toast.error("Failed to delete restaurant", {
        position: toast.POSITION.TOP_RIGHT,
      });
    } else {
      toast.success("Restaurant deleted successfully!", {
        position: toast.POSITION.TOP_RIGHT,
      });
      history.push("/secure/restaurants");
    }
  };
  const onSelectMeal = (item: any) => {
    setEdit(item);
    setMealName(item.name);
    setMealDesc(item.description);
    setMealPrice(item.price);
  };
  return (
    <div className="container-big">
      <ToastContainer />
      <Grid container spacing={2} alignItems="flex-start">
        <Grid item xs={8} className="menu">
          <section className={classes.section}>
            <h2>{name}</h2>{" "}
            <Tooltip title="Edit Restaurant">
              <Fab
                color="primary"
                aria-label="add"
                className={classes.editIcon}
                onClick={onEditRestaurant}
              >
                <EditIcon />
              </Fab>
            </Tooltip>
            <Tooltip title="Delete Restaurant">
              <Fab
                color="primary"
                aria-label="add"
                className={classes.deleteIcon}
                onClick={onRemoveRestaurant}
              >
                <RemoveIcon />
              </Fab>
            </Tooltip>
            <h3 className="gray">{description}</h3>
          </section>
          <h3>Menu of {name}</h3>
          <Tooltip title="Add Meal">
            <Fab
              color="primary"
              aria-label="add"
              onClick={() =>
                onSelectMeal({
                  id: "new",
                  name: "",
                  description: "",
                  price: "",
                })
              }
            >
              <AddIcon />
            </Fab>
          </Tooltip>

          {meals.map((item: any) => (
            <MealEdit
              onDelete={onDeleteMeal}
              onEdit={onSelectMeal}
              item={item}
              key={item.name}
            />
          ))}
        </Grid>
        <Grid item xs={4} style={{ paddingTop: "20%" }}>
          {editRestaurant ? (
            <form className={classes.root} noValidate autoComplete="off">
              <Typography
                style={{ paddingLeft: 20 }}
                className="gray"
                component="h6"
                variant="h6"
              >
                Update Restaurant Details
              </Typography>
              <TextField
                error={restaurantName === ""}
                id="filled-error"
                label="Name"
                variant="filled"
                value={restaurantName}
                onChange={(e) => setRestaurantName(e.target.value)}
                helperText={restaurantName === "" ? "Required" : ""}
                fullWidth
              />
              <TextField
                error={restaurantDesc === ""}
                id="filled-error"
                label="Description"
                variant="filled"
                multiline
                rows={8}
                value={restaurantDesc}
                onChange={(e) => setRestaurantDesc(e.target.value)}
                helperText={restaurantDesc === "" ? "Required" : ""}
                fullWidth
              />
              <div className={classes.row}>
                <Button
                  onClick={onUpdateRestaurant}
                  variant="contained"
                  color="primary"
                  fullWidth
                >
                  Update
                </Button>
                <Button
                  onClick={() => setEditRestaurant(false)}
                  variant="contained"
                  color="primary"
                  fullWidth
                >
                  Cancel
                </Button>
              </div>
            </form>
          ) : null}
          {edit?.id ? (
            <>
              <form className={classes.root} noValidate autoComplete="off">
                <Typography
                  style={{ paddingLeft: 20 }}
                  className="gray"
                  component="h6"
                  variant="h6"
                >
                  {edit.id === "new" ? "Add new meal" : "Update meal"}
                </Typography>
                <TextField
                  error={mealName === ""}
                  id="filled-error"
                  label="Name"
                  variant="filled"
                  value={mealName}
                  onChange={(e) => setMealName(e.target.value)}
                  helperText={mealName === "" ? "Required" : ""}
                  fullWidth
                />
                <TextField
                  error={mealDesc === ""}
                  id="filled-error-helper-text"
                  label="Description"
                  helperText="Required"
                  variant="filled"
                  value={mealDesc}
                  fullWidth
                  multiline
                  rows={8}
                  onChange={(e) => setMealDesc(e.target.value)}
                />
                <TextField
                  id="filled-error-helper-text"
                  fullWidth
                  label="Price ($)"
                  variant="filled"
                  type="number"
                  value={mealPrice}
                  onChange={(e) => setMealPrice(e.target.value)}
                />
                <div className={classes.row}>
                  <Button
                    fullWidth
                    onClick={edit.id === "new" ? onCreateMeal : onUpdateMeal}
                    variant="contained"
                    color="primary"
                  >
                    {edit.id === "new" ? "Add" : "Update"}
                  </Button>
                  <Button
                    onClick={() => setEdit({})}
                    variant="contained"
                    color="primary"
                    fullWidth
                  >
                    Cancel
                  </Button>
                </div>
              </form>
            </>
          ) : null}
        </Grid>
      </Grid>
    </div>
  );
};
export default MealManager;
