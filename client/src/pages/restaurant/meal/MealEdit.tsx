import React from "react";
import { gql } from "@apollo/client";
import { MenuItemEdit } from "../../../components";
export const QUERY = gql`
  {
    restaurants {
      name
      description
      createdAt
      updatedAt
      id
      meals {
        name
        price
        description
      }
    }
  }
`;
interface IProps {
  item: any;
  onEdit: (id: any) => void;
  onDelete: (id: any) => void;
}

const Meal = ({ onEdit, onDelete, item }: IProps) => {
  return (
    <MenuItemEdit
      onDelete={onDelete}
      onEdit={onEdit}
      id={item.id}
      name={item.name}
      description={item.description}
      price={item.price}
      quantity={item.quantity}
    />
  );
};
export default Meal;
