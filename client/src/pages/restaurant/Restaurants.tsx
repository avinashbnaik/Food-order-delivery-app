import React, { useState } from "react";
import { gql, useQuery, useMutation } from "@apollo/client";
import RestaurantCard from "./RestaurantCard";
import { Restaurant } from "./types";
import { Grid, Button, Typography, Tooltip } from "@material-ui/core/";
import { PostsContext } from "../../Context";
import TextField from "@material-ui/core/TextField";
import { ROLES } from "../../utils";
import { ToastContainer, toast } from "react-toastify";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";

import "react-toastify/dist/ReactToastify.css";

import { useHistory } from "react-router-dom";
import "./index.css";

export const QUERY = gql`
  {
    restaurants {
      name
      description
      createdAt
      updatedAt
      id
      meals {
        name
        price
        description
      }
    }
  }
`;
export const OWNER_QUERY = gql`
  {
    me {
      restaurants {
        name
        description
        createdAt
        updatedAt
        id
        meals {
          name
          price
          description
        }
      }
    }
  }
`;

export const CREATE_RESTAURANT = gql`
  mutation createRestaurant($name: String, $description: String) {
    createRestaurant(name: $name, description: $description) {
      name
      description
      id
    }
  }
`;
const Restaurants = () => {
  const { data: { restaurants = [] } = {} } = useQuery(QUERY, {
    fetchPolicy: "network-only",
  });
  const { data: { me: { restaurants: ownerRestaurants = [] } = {} } = {} } =
    useQuery(OWNER_QUERY, {
      fetchPolicy: "network-only",
    });
  const history = useHistory();
  const context = React.useContext(PostsContext);
  const [restaurantName, setRestaurantName] = useState<string>("");
  const [restaurantDesc, setRestaurantDesc] = useState<string>("");
  const [createRestaurant] = useMutation(CREATE_RESTAURANT);
  const [showAddRestaurant, setShowAddRestaurant] = useState<boolean>(false);
  const allRestaurants =
    context?.user?.role === ROLES.RESTAURANT_OWNER
      ? ownerRestaurants
      : restaurants;

  // useEffect(() => {

  //   fetchRestaurants()
  // },[])
  const onAdd = async () => {
    try {
      const { data: { createRestaurant: { id = "" } = {} } = {}, errors = [] } =
        await createRestaurant({
          variables: {
            name: restaurantName,
            description: restaurantDesc,
          },
        });

      if (errors.length > 0) {
        toast.error("Failed to create Restaurant", {
          position: toast.POSITION.TOP_RIGHT,
        });
      } else {
        toast.success("Restaurant created successfully!", {
          position: toast.POSITION.TOP_RIGHT,
        });
        history.push(`/secure/restaurant/${id}`);
      }
    } catch (e) {
      e.graphQLErrors.some((graphQLError: any) =>
        toast.error(graphQLError.message, {
          position: toast.POSITION.TOP_RIGHT,
        })
      );
    }
  };
  return (
    <div className="container">
      <h3>Restaurants</h3>
      <ToastContainer />

      <Grid container spacing={2}>
        {allRestaurants?.length ? (
          allRestaurants.map((restaurant: Restaurant) => (
            <Grid key={restaurant.id} item xs={12} sm={4}>
              <RestaurantCard
                title={restaurant.name}
                description={restaurant.description}
                onClick={() =>
                  history.push(`/secure/restaurant/${restaurant.id}`)
                }
              />
            </Grid>
          ))
        ) : (
          <Typography className="gray">No Restaurants found</Typography>
        )}
      </Grid>
      {context?.user?.role === ROLES.RESTAURANT_OWNER ? (
        <div className="add-card">
          <Tooltip title="Add Restaurant">
            <Fab
              color="primary"
              aria-label="add"
              onClick={() => setShowAddRestaurant(true)}
            >
              <AddIcon />
            </Fab>
          </Tooltip>
        </div>
      ) : null}
      {context?.user?.role === ROLES.RESTAURANT_OWNER && showAddRestaurant ? (
        <form noValidate autoComplete="off" className="form-add-restaurant">
          <Typography
            style={{ paddingLeft: 20 }}
            className="gray"
            component="h6"
            variant="h6"
          >
            Add Restaurant
          </Typography>
          <TextField
            error={restaurantName === ""}
            id="filled-error"
            label="Name"
            variant="filled"
            value={restaurantName}
            onChange={(e) => setRestaurantName(e.target.value)}
            helperText={restaurantName === "" ? "Required" : ""}
            fullWidth
          />
          <TextField
            error={restaurantDesc === ""}
            id="filled-error"
            label="Description"
            variant="filled"
            multiline
            rows={8}
            value={restaurantDesc}
            onChange={(e) => setRestaurantDesc(e.target.value)}
            helperText={restaurantDesc === "" ? "Required" : ""}
            fullWidth
          />
          <div
            style={{
              display: "flex",
              justifyContent: "space-around",
              alignItems: "center",
              padding: 8,
              gap: 8,
            }}
          >
            <Button
              onClick={onAdd}
              variant="contained"
              color="primary"
              disabled={restaurantDesc === "" || restaurantName === ""}
              fullWidth
            >
              Add
            </Button>
            <Button
              onClick={() => setShowAddRestaurant(false)}
              variant="contained"
              color="primary"
              fullWidth
            >
              Cancel
            </Button>
          </div>
        </form>
      ) : null}
    </div>
  );
};
export default Restaurants;
