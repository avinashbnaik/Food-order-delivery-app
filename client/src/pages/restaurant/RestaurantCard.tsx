import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
// import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
// import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { Tooltip } from "@material-ui/core";

const useStyles = makeStyles({
  root: {
    maxWidth: 500,
    padding: 8,
    height: 400,
    overflow: "hidden",
    textOverflow: "ellipsis",
  },
});
interface IProps {
  title: string;
  description: string;
  onClick: () => void;
}

export default function RestaurantCard({
  title,
  description,
  onClick,
}: IProps) {
  const classes = useStyles();

  return (
    <Card className={classes.root} onClick={onClick}>
      <img
        src={`https://loremflickr.com/320/240/${title}`}
        alt="pizza"
        height="200"
        width="300"
      />
      <CardActionArea>
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {title}
          </Typography>
          <Tooltip title={description}>
            <Typography variant="body2" color="textSecondary" component="p">
              {description.slice(0, 100)}
              {description.length > 100 ? "..." : null}
            </Typography>
          </Tooltip>
        </CardContent>
      </CardActionArea>
    </Card>
  );
}
