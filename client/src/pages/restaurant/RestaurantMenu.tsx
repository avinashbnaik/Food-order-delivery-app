import React, { useEffect, useState } from "react";
import { gql, useQuery, useMutation } from "@apollo/client";
import "./index.css";
import Meal from "./meal/Meal";
import Table from "../../components/Table";
import Grid from "@material-ui/core/Grid";
import { useSelector } from "react-redux";
import { ToastContainer, toast } from "react-toastify";
import { useHistory } from "react-router-dom";
import { deleteRestaurant } from "../../store/actions/menu";
import { useDispatch } from "react-redux";

import "react-toastify/dist/ReactToastify.css";
export const QUERY = gql`
  query restaurants($id: ID) {
    restaurantById(id: $id) {
      name
      description
      createdAt
      updatedAt
      id
      meals {
        id
        name
        price
        description
      }
    }
  }
`;

export const CREATE_ORDER_QUERY = gql`
  mutation createOrder($restaurantId: ID!, $items: [OrderItem]) {
    createOrder(restaurantId: $restaurantId, items: $items) {
      id
    }
  }
`;
const RestaurantMenu = (props: any) => {
  const {
    data: {
      restaurantById: { name = "", description = "", meals = [], id = "" } = {},
    } = {},
  } = useQuery(QUERY, { variables: { id: props.match.params.id } });
  const history = useHistory();
  const [createOrder] = useMutation(CREATE_ORDER_QUERY);
  const [orderItems, setOrderItems] = useState<any>([]);
  const dispatch = useDispatch();

  const menuOrderItems: any = useSelector<any>(
    ({ menu: { orderItems = {} } = {} }) => orderItems
  );
  useEffect(() => {
    if (menuOrderItems[id]) {
      setOrderItems(menuOrderItems[id]);
    }
  }, [menuOrderItems[id]]);
  const onCreateOrder = async () => {
    const {
      data: { createOrder: { id: orderId = "" } = {} } = {},
      errors = [],
    } = await createOrder({
      variables: {
        restaurantId: id,
        items: orderItems.map((item: any) => ({
          mealId: item.id,
          quantity: item.quantity,
        })),
      },
    });

    if (errors.length > 0) {
      toast.error("Failed to create order", {
        position: toast.POSITION.TOP_RIGHT,
      });
    } else {
      toast.success("Order created successfully!", {
        position: toast.POSITION.TOP_RIGHT,
      });
      dispatch(deleteRestaurant({ restaurantId: id }));
      history.push(`/secure/order/${orderId}`);
    }
  };
  return (
    <div className="container-big">
      <ToastContainer />
      <Grid container spacing={2} alignItems="flex-start">
        <Grid item md={8} xs={12} className="menu">
          <section>
            <h2>{name}</h2>
            <h3 className="gray">{description}</h3>
          </section>
          <h3>Menu of {name}</h3>
          {meals.map((item: any) => (
            <Meal item={item} restaurantId={id} key={item.name} />
          ))}
        </Grid>
        <Grid item xs={12} md={4} style={{ paddingTop: "20px" }}>
          {orderItems.length > 0 ? (
            <Table items={orderItems} onSubmit={onCreateOrder} />
          ) : null}
        </Grid>
      </Grid>
    </div>
  );
};
export default RestaurantMenu;
