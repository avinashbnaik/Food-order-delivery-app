import React from "react";
import { TextField, Button } from "@material-ui/core/";
import { gql, useMutation } from "@apollo/client";
import { Typography } from "@material-ui/core";
import { PostsContext } from "../../Context";
import { useHistory } from "react-router-dom";

const MUTATION = gql`
  mutation LoginUser($email: String!, $password: String!) {
    loginUser(email: $email, password: $password) {
      token
      user {
        id
        name
        email
        role
      }
    }
  }
`;
const Login = () => {
  const history = useHistory();
  const context = React.useContext(PostsContext);
  const [message, setMessage] = React.useState("");
  const [email, setEmail] = React.useState("");

  const [password, setPassword] = React.useState("");
  const [loginUser] = useMutation(MUTATION);

  const loginF = async () => {
    let dataUser;
    try {
      dataUser = await loginUser({
        variables: {
          email,
          password,
        },
      });
    } catch (e) {
      e.graphQLErrors.some((graphQLError: any) =>
        setMessage(graphQLError.message)
      );
    }
    if (dataUser?.data?.loginUser) {
      setMessage("");
      localStorage.setItem("AUTH_TOKEN", dataUser.data.loginUser.token);
      context.updateUser(dataUser.data.loginUser.user);
      history.push("/");
    }
  };
  const handleKeyPress = (e: any) => {
    if (e.key === "Enter") {
      loginF();
    }
  };
  return (
    <div
      className="container"
      style={{
        marginTop: 170,
        display: "flex",
        justifyContent: "center",
        gap: 8,
        padding: 8,
        flexDirection: "column",
        maxWidth: 500,
      }}
    >
      <Typography component="h5" variant="h5">
        Login
      </Typography>

      <div>
        <TextField
          id="email"
          label="email"
          variant="filled"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
      </div>
      <div>
        <TextField
          id="password"
          label="password"
          type={"password"}
          variant="filled"
          onKeyPress={handleKeyPress}
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
      </div>
      <div style={{ height: "15px" }} />
      <div>
        <Button variant={"outlined"} color={"primary"} onClick={loginF}>
          Login
        </Button>
      </div>

      <Typography color={"secondary"}>{message}</Typography>
    </div>
  );
};
export default Login;
