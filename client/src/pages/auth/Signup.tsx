import React from "react";
import { TextField, Button } from "@material-ui/core/";
import { gql, useMutation } from "@apollo/client";
import { Typography } from "@material-ui/core";
import { PostsContext } from "../../Context";
import { useHistory } from "react-router-dom";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import { ROLES } from "../../utils";

const MUTATION = gql`
  mutation SignupUser(
    $name: String!
    $email: String!
    $password: String!
    $role: String
  ) {
    signupUser(name: $name, email: $email, password: $password, role: $role) {
      token
      user {
        id
        name
        email
        role
      }
    }
  }
`;
export default function Signup() {
  const history = useHistory();
  const context = React.useContext(PostsContext);
  const [message, setMessage] = React.useState("");
  const [email, setEmail] = React.useState("");
  const [name, setName] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [signupUser] = useMutation(MUTATION);
  const [role, setRole] = React.useState(ROLES.USER);
  React.useEffect(() => {
    if (context.user.id) {
      history.push("/");
    }
  }, [context]);

  const handleKeyPress = (e: any) => {
    if (e.key === "Enter") {
      signupF();
    }
  };

  const signupF = async () => {
    let dataUser;
    try {
      dataUser = await signupUser({
        variables: {
          name,
          email,
          password,
          role,
        },
      });
    } catch (e) {
      e.graphQLErrors.some((graphQLError: any) =>
        setMessage(graphQLError.message)
      );
    }
    if (dataUser?.data?.signupUser) {
      setMessage("");
      localStorage.setItem("AUTH_TOKEN", dataUser.data.signupUser.token);
      context.updateUser(dataUser.data.signupUser.user);
      history.push("/");
    }
  };

  return (
    <div
      className="container"
      style={{
        marginTop: 170,
        display: "flex",
        justifyContent: "center",
        gap: 8,
        padding: 8,
        flexDirection: "column",
        maxWidth: 500,
      }}
    >
      <Typography component="h5" variant="h5">
        Signup{" "}
      </Typography>
      <div style={{}}>
        <TextField
          fullWidth
          id="name"
          label="Name"
          variant="filled"
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
      </div>
      <div>
        <TextField
          id="email"
          fullWidth
          variant="filled"
          label="Email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
      </div>
      <div>
        <TextField
          id="password"
          fullWidth
          label="Password"
          variant="filled"
          type={"password"}
          onKeyPress={handleKeyPress}
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
      </div>
      <FormControlLabel
        control={
          <Checkbox
            checked={role === ROLES.RESTAURANT_OWNER}
            onChange={() =>
              setRole((p) =>
                p === ROLES.USER ? ROLES.RESTAURANT_OWNER : ROLES.USER
              )
            }
            name="isOwner"
            color="primary"
          />
        }
        label="isOwner"
      />

      <div style={{ height: "15px" }} />
      <div>
        <Button variant={"outlined"} color={"primary"} onClick={signupF}>
          Sign up
        </Button>
      </div>
      <Typography color={"secondary"}>{message}</Typography>
    </div>
  );
}
