import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import Header from "./Header";
import Me from "./Me";
import { Provider } from "react-redux";

import store from "./store";
import { PostsContext, postsContextDefaultValue } from "./Context";
import RouteApp from "./route/RouteApp";

export default function BasicExample() {
  const [user, setUser] = React.useState(postsContextDefaultValue.user);

  const updateUser = React.useCallback(
    (user: any) => {
      setUser(user);
    },
    [setUser, user]
  );

  return (
    <Provider store={store}>
      <PostsContext.Provider value={{ user, updateUser }}>
        <Router>
          <Header />
          <Me />
          <RouteApp />
        </Router>
      </PostsContext.Provider>
    </Provider>
  );
}
